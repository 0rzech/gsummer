header =
  .reset_tooltip = Zresetuj program
  .about_tooltip = Pokaż informacje o programie

file_chooser =
  .choose_file = Wybierz _plik
  .choose = Wybierz
  .cancel = Anuluj

# $algorithm (String)
algorithm =
  .safe = 💚 {$algorithm}
  .moderate = 💛 {$algorithm}
  .compromised = 💔 {$algorithm}

main =
  .algorithm_tooltip = Wybierz algorytm obliczania sumy kontrolnej
  .edit_placeholder = Wprowadź tutaj sumę kontrolną
  .calculate = _Oblicz
  .verify = _Zweryfikuj
  .cancel = _Anuluj

status =
  .checksums_match = Sumy kontrolne się zgadzają
  .checksums_do_not_match = Sumy kontrolne się NIE zgadzają

about_dialog =
  .description = Suma jest prostym walidatorem sum kontrolnych.
  .copyright = Prawa autorskie © 2013-2022 Piotr Orzechowski [orzechowski.tech]

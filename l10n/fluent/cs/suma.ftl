header =
  .reset_tooltip = Resetujte program
  .about_tooltip = Zobrazit informace o programu

file_chooser =
  .choose_file = Vyberte _soubor
  .choose = Vybrat
  .cancel = Zrušit

# $algorithm (String)
algorithm =
  .safe = 💚 {$algorithm}
  .moderate = 💛 {$algorithm}
  .compromised = 💔 {$algorithm}

main =
  .algorithm_tooltip = Vyberte algoritmus pro výpočet kontrolního součtů
  .edit_placeholder = Zde zadejte kontrolní součet
  .calculate = _Vypočítejte
  .verify = _Ověřit
  .cancel = _Zrušit

status =
  .checksums_match = Kontrolní součty se shodují
  .checksums_do_not_match = Kontrolní součty SE NESHODUJÍ

about_dialog =
  .description = Suma je prostým schvalovačem kontrolních součtů.
  .copyright = Autorské právo © 2013-2022 Piotr Orzechowski [orzechowski.tech]

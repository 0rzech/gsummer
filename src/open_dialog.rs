use gtk::prelude::*;
use relm4::{gtk, ComponentParts, ComponentSender, SimpleComponent};
use std::path::PathBuf;

pub struct OpenDialogSettings {
    pub accept_label: String,
    pub cancel_label: String,
    pub create_folders: bool,
    pub filters: Vec<gtk::FileFilter>,
    pub modal: bool,
}

#[derive(Debug)]
pub enum OpenDialogInput {
    Open,
    Accept(PathBuf),
    InvalidSelection,
    Cancel,
}

#[derive(Debug)]
pub enum OpenDialogOutput {
    Accept(PathBuf),
}

#[tracker::track]
pub struct OpenDialog {
    visible: bool,
}

#[relm4::component(pub)]
impl SimpleComponent for OpenDialog {
    type Init = OpenDialogSettings;
    type Input = OpenDialogInput;
    type Output = OpenDialogOutput;

    fn init(
        settings: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = Self {
            visible: false,
            tracker: 0,
        };

        let widgets = view_output!();

        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        #[name = "file_chooser"]
        gtk::FileChooserNative {
            set_accept_label: Some(&settings.accept_label),
            set_cancel_label: Some(&settings.cancel_label),
            set_create_folders: settings.create_folders,
            #[iterate]
            add_filter: &settings.filters,
            set_modal: settings.modal,
            #[track = "model.changed(OpenDialog::visible())"]
            set_visible: model.visible,
            set_action: gtk::FileChooserAction::Open,

            connect_response => move |dialog, response| {
                match response {
                    gtk::ResponseType::Accept => {
                        let path = if let Some(file) = dialog.file() {
                            file.path()
                        } else {
                            None
                        };

                        match path {
                            Some(path) => sender.input(Self::Input::Accept(path)),
                            None => sender.input(Self::Input::InvalidSelection),
                        }
                    },
                    gtk::ResponseType::Cancel => sender.input(Self::Input::Cancel),
                    _ => (),
                }
            },
        }
    }

    fn update(&mut self, message: Self::Input, sender: ComponentSender<Self>) {
        self.reset();
        match message {
            Self::Input::Open => self.set_visible(true),
            Self::Input::Accept(path) => {
                self.set_visible(false);
                sender.output(Self::Output::Accept(path)).unwrap();
            }
            Self::Input::Cancel => self.set_visible(false),
            _ => (),
        }
    }
}

use digest::{Digest, DynDigest};
use md5::Md5;
use sha1::Sha1;
use sha2::{Sha224, Sha256, Sha384, Sha512};
use sha3::{Sha3_224, Sha3_256, Sha3_384, Sha3_512};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Algorithm {
    Md5,
    Sha1,
    Sha224,
    Sha256,
    Sha384,
    Sha512,
    Sha3_224,
    Sha3_256,
    Sha3_384,
    Sha3_512,
}

pub trait AlgorithmTextDecorator {
    fn decorate_safe_algorithm(&self, algorithm: &str) -> String;
    fn decorate_moderate_algorithm(&self, algorithm: &str) -> String;
    fn decorate_compromised_algorithm(&self, algorithm: &str) -> String;
}

pub type Hasher = Box<dyn DynDigest>;

impl Default for Algorithm {
    fn default() -> Self {
        Self::Sha512
    }
}

const MD5: &str = "MD5";
const SHA1: &str = "SHA-1";
const SHA224: &str = "SHA-224";
const SHA256: &str = "SHA-256";
const SHA384: &str = "SHA-384";
const SHA512: &str = "SHA-512";
const SHA3_224: &str = "SHA3-224";
const SHA3_256: &str = "SHA3-256";
const SHA3_384: &str = "SHA3-384";
const SHA3_512: &str = "SHA3-512";

impl Algorithm {
    pub fn to_string<Decorator>(self, decorator: &Decorator) -> String
    where
        Decorator: AlgorithmTextDecorator,
    {
        match self {
            Self::Md5 => decorator.decorate_compromised_algorithm(MD5),
            Self::Sha1 => decorator.decorate_compromised_algorithm(SHA1),
            Self::Sha224 => decorator.decorate_safe_algorithm(SHA224),
            Self::Sha256 => decorator.decorate_safe_algorithm(SHA256),
            Self::Sha384 => decorator.decorate_safe_algorithm(SHA384),
            Self::Sha512 => decorator.decorate_safe_algorithm(SHA512),
            Self::Sha3_224 => decorator.decorate_safe_algorithm(SHA3_224),
            Self::Sha3_256 => decorator.decorate_safe_algorithm(SHA3_256),
            Self::Sha3_384 => decorator.decorate_safe_algorithm(SHA3_384),
            Self::Sha3_512 => decorator.decorate_safe_algorithm(SHA3_512),
        }
    }

    pub fn from_str(str: &str) -> Self {
        if str.contains(MD5) {
            Self::Md5
        } else if str.contains(SHA1) {
            Self::Sha1
        } else if str.contains(SHA224) {
            Self::Sha224
        } else if str.contains(SHA256) {
            Self::Sha256
        } else if str.contains(SHA384) {
            Self::Sha384
        } else if str.contains(SHA512) {
            Self::Sha512
        } else if str.contains(SHA3_224) {
            Self::Sha3_224
        } else if str.contains(SHA3_256) {
            Self::Sha3_256
        } else if str.contains(SHA3_384) {
            Self::Sha3_384
        } else if str.contains(SHA3_512) {
            Self::Sha3_512
        } else {
            panic!("Invalid checksum string variant: {}", str)
        }
    }

    pub fn new_hasher(&self) -> Hasher {
        match self {
            Self::Md5 => Box::new(Md5::new()),
            Self::Sha1 => Box::new(Sha1::new()),
            Self::Sha224 => Box::new(Sha224::new()),
            Self::Sha256 => Box::new(Sha256::new()),
            Self::Sha384 => Box::new(Sha384::new()),
            Self::Sha512 => Box::new(Sha512::new()),
            Self::Sha3_224 => Box::new(Sha3_224::new()),
            Self::Sha3_256 => Box::new(Sha3_256::new()),
            Self::Sha3_384 => Box::new(Sha3_384::new()),
            Self::Sha3_512 => Box::new(Sha3_512::new()),
        }
    }
}

#[cfg(test)]
mod tests;

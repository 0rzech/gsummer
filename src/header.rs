use gtk::prelude::*;
use relm4::{gtk, ComponentParts, ComponentSender, SimpleComponent};

pub struct HeaderSettings {
    pub reset_button_tooltip: Option<String>,
    pub about_button_tooltip: Option<String>,
}

#[derive(Debug)]
pub enum HeaderInput {
    SetSensitive(bool),
}

#[derive(Debug)]
pub enum HeaderOutput {
    ResetState,
    ShowAboutDialog,
}

#[tracker::track]
pub struct Header {
    sensitive: bool,
}

#[relm4::component(pub)]
impl SimpleComponent for Header {
    type Init = HeaderSettings;
    type Input = HeaderInput;
    type Output = HeaderOutput;

    fn init(
        settings: Self::Init,
        root: &Self::Root,
        sender: ComponentSender<Self>,
    ) -> ComponentParts<Self> {
        let model = Self {
            sensitive: true,
            tracker: 0,
        };

        let widgets = view_output!();

        if let Some(text) = &settings.reset_button_tooltip {
            widgets.new_button.set_tooltip_text(Some(text));
        }

        if let Some(text) = &settings.about_button_tooltip {
            widgets.about_button.set_tooltip_text(Some(text));
        }

        ComponentParts { model, widgets }
    }

    view! {
        #[root]
        gtk::HeaderBar {
            #[name = "new_button"]
            pack_start = &gtk::Button {
                set_icon_name: "document-new-symbolic",

                #[track = "model.changed(Header::sensitive())"]
                set_sensitive: model.sensitive,

                connect_clicked[sender] => move |_| {
                    sender.output(Self::Output::ResetState).unwrap();
                },
            },
            #[name = "about_button"]
            pack_end = &gtk::Button {
                set_icon_name: "help-about-symbolic",

                connect_clicked[sender] => move |_| {
                    sender.output(Self::Output::ShowAboutDialog).unwrap();
                }
            }
        }
    }

    fn update(&mut self, message: Self::Input, _sender: ComponentSender<Self>) {
        self.reset();
        match message {
            Self::Input::SetSensitive(sensitive) => self.set_sensitive(sensitive),
        }
    }
}

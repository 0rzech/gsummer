use crate::config;

use config::LocaleConfig;
use fluent_bundle::{FluentArgs, FluentBundle, FluentResource};
use fluent_langneg::{negotiate_languages, NegotiationStrategy};
use std::{
    borrow::Borrow,
    fs,
    path::{Path, PathBuf},
    result::Result,
};
use sys_locale::get_locale;
use unic_langid::LanguageIdentifier;

pub fn load(config: &LocaleConfig) -> Result<FluentBundle<FluentResource>, String> {
    let default_loc_id: LanguageIdentifier = config
        .default_locale
        .parse()
        .unwrap_or_else(|_| panic!("Unable to parse default locale {}", config.default_locale));

    let system_loc_id = get_system_locale(&default_loc_id, get_locale);

    let is_file = |template, locale, filename| file_path(template, locale, filename).is_file();

    let available_loc_ids = get_available_locales(
        config.supported_locales,
        config.locale_path_template,
        config.locale_filename,
        is_file,
    )
    .unwrap();

    let best_match = (*negotiate_languages(
        &[system_loc_id],
        &available_loc_ids,
        Some(&default_loc_id),
        NegotiationStrategy::Filtering,
    )
    .first()
    .unwrap())
    .clone();

    let path = file_path(
        config.locale_path_template,
        &best_match.to_string(),
        config.locale_filename,
    );

    Ok(load_bundle(best_match, &path))
}

fn get_system_locale<F>(default_locale_id: &LanguageIdentifier, get_locale: F) -> LanguageIdentifier
where
    F: Fn() -> Option<String>,
{
    if let Some(locale) = get_locale() {
        if let Ok(loc) = locale.parse() {
            loc
        } else {
            eprintln!(
                "Unable to parse system locale: `{}`; assuming default of: `{}`",
                locale, default_locale_id
            );
            default_locale_id.clone()
        }
    } else {
        eprintln!(
            "Unable to obtain system locale; assuming default of `{}`",
            default_locale_id
        );
        default_locale_id.clone()
    }
}

fn file_path(template: &str, locale: &str, filename: &str) -> PathBuf {
    PathBuf::from(
        template
            .replace("{locale}", locale)
            .replace("{res_id}", filename),
    )
}

fn get_available_locales<'a, F>(
    supported_locales: &'a [&str],
    locale_path_template: &'a str,
    filename: &'a str,
    is_file: F,
) -> Result<Vec<LanguageIdentifier>, String>
where
    F: Fn(&'a str, &'a str, &'a str) -> bool + Copy,
{
    let mut locales = vec![];

    for supported in supported_locales {
        if is_file(locale_path_template, supported, filename) {
            locales.push(
                supported
                    .parse()
                    .unwrap_or_else(|_| panic!("Unable to parse locale {}", supported)),
            );
        }
    }

    if locales.is_empty() {
        return Err("None of supported locales found on filesystem".to_string());
    }

    Ok(locales)
}

fn load_bundle(language_id: LanguageIdentifier, path: &Path) -> FluentBundle<FluentResource> {
    let contents = fs::read_to_string(&path)
        .unwrap_or_else(|error| panic!("Unable to read `{}`: {}", path.display(), error));
    let resource = FluentResource::try_new(contents)
        .unwrap_or_else(|errors| panic!("Unable to load fluent resource:\n{:#?}", errors));
    let mut bundle = FluentBundle::new(vec![language_id]);

    bundle.add_resource(resource).unwrap_or_else(|errors| {
        panic!(
            "Unable to add fluent resource to fluent bundle:\n{:#?}",
            errors
        )
    });

    bundle
}

pub trait FluentBundleExt {
    fn get_message_attribute(
        &self,
        msg_key: &str,
        attr_key: &str,
        args: Option<&FluentArgs>,
    ) -> String;
}

impl<R> FluentBundleExt for FluentBundle<R>
where
    R: Borrow<FluentResource>,
{
    fn get_message_attribute(
        &self,
        msg_key: &str,
        attr_key: &str,
        args: Option<&FluentArgs>,
    ) -> String {
        let message = self
            .get_message(msg_key)
            .unwrap_or_else(|| panic!("Unable to find message with `{}` key", msg_key));
        let mut errors = vec![];

        let attribute_value;
        if let Some(attribute) = message.get_attribute(attr_key) {
            attribute_value = self
                .format_pattern(attribute.value(), args, &mut errors)
                .to_string();
        } else {
            attribute_value = format!("{}.{}", msg_key, attr_key);
            eprintln!("No attribute value for `{}.{}` key", msg_key, attr_key);
        }

        if !errors.is_empty() {
            eprintln!("{:?}", errors);
        }

        attribute_value
    }
}

#[cfg(test)]
mod tests;

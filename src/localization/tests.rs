use std::path::PathBuf;
use unic_langid::LanguageIdentifier;

#[test]
fn get_system_locale() {
    let default_locale_id = "pl".parse().unwrap();

    assert_eq!(
        super::get_system_locale(&default_locale_id, || Some("en".to_string())),
        "en".parse::<LanguageIdentifier>().unwrap()
    );
    assert_eq!(
        super::get_system_locale(&default_locale_id, || Some("unparsable".to_string())),
        default_locale_id
    );
    assert_eq!(
        super::get_system_locale(&default_locale_id, || None),
        default_locale_id
    )
}

#[test]
fn file_path() {
    let template = "/path/to/{locale}/with/{res_id}";
    let locale = "pl";
    let res_id = "main.ftl";

    assert_eq!(
        super::file_path(template, locale, res_id),
        PathBuf::from("/path/to/pl/with/main.ftl")
    )
}

#[test]
fn get_available_locales() {
    let supported_locales = vec!["cs", "en", "pl"];
    let is_file = |_, locale, _| matches!(locale, "cs" | "pl");

    assert_eq!(
        super::get_available_locales(&supported_locales, "", "", is_file),
        Ok(vec![
            "cs".parse::<LanguageIdentifier>().unwrap(),
            "pl".parse().unwrap()
        ])
    );
    assert!(
        super::get_available_locales(&supported_locales, "", "", |_, _, _| false).is_err(),
        "Did not return `Err` when no supported locale found"
    )
}

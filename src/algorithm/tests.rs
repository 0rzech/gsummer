use super::*;

#[test]
fn default() {
    assert_eq!(Algorithm::default(), Algorithm::Sha512);
}

#[test]
fn to_string() {
    let decorator = Decorator {
        safe: "Safe",
        moderate: "Moderate",
        compromised: "Compromised",
    };

    assert_eq!(Algorithm::Md5.to_string(&decorator), "Compromised MD5");
    assert_eq!(Algorithm::Sha1.to_string(&decorator), "Compromised SHA-1");
    assert_eq!(Algorithm::Sha224.to_string(&decorator), "Safe SHA-224");
    assert_eq!(Algorithm::Sha256.to_string(&decorator), "Safe SHA-256");
    assert_eq!(Algorithm::Sha384.to_string(&decorator), "Safe SHA-384");
    assert_eq!(Algorithm::Sha512.to_string(&decorator), "Safe SHA-512");
    assert_eq!(Algorithm::Sha3_224.to_string(&decorator), "Safe SHA3-224");
    assert_eq!(Algorithm::Sha3_256.to_string(&decorator), "Safe SHA3-256");
    assert_eq!(Algorithm::Sha3_384.to_string(&decorator), "Safe SHA3-384");
    assert_eq!(Algorithm::Sha3_512.to_string(&decorator), "Safe SHA3-512");
}

#[test]
fn from_str_with_valid_input() {
    assert_eq!(Algorithm::from_str("Some MD5 algorithm"), Algorithm::Md5);
    assert_eq!(Algorithm::from_str("Some SHA-1 algorithm"), Algorithm::Sha1);
    assert_eq!(
        Algorithm::from_str("Some SHA-224 algorithm"),
        Algorithm::Sha224
    );
    assert_eq!(
        Algorithm::from_str("Some SHA-256 algorithm"),
        Algorithm::Sha256
    );
    assert_eq!(
        Algorithm::from_str("Some SHA-384 algorithm"),
        Algorithm::Sha384
    );
    assert_eq!(
        Algorithm::from_str("Some SHA-512 algorithm"),
        Algorithm::Sha512
    );
    assert_eq!(
        Algorithm::from_str("Some SHA3-224 algorithm"),
        Algorithm::Sha3_224
    );
    assert_eq!(
        Algorithm::from_str("Some SHA3-256 algorithm"),
        Algorithm::Sha3_256
    );
    assert_eq!(
        Algorithm::from_str("Some SHA3-384 algorithm"),
        Algorithm::Sha3_384
    );
    assert_eq!(
        Algorithm::from_str("Some SHA3-512 algorithm"),
        Algorithm::Sha3_512
    );
}

#[test]
#[should_panic]
fn from_str_with_invalid_input() {
    Algorithm::from_str("Some unknown algorithm");
}

#[test]
fn new_hasher() {
    let data = b"Suma";

    assert_eq!(
        hash(Algorithm::Md5.new_hasher(), data),
        "e56721abb63b2b1039567813642d8b49"
    );
    assert_eq!(
        hash(Algorithm::Sha1.new_hasher(), data),
        "fe72dd59d42e065d8ce9fa5e144019e99a632198"
    );
    assert_eq!(
        hash(Algorithm::Sha224.new_hasher(), data),
        "af6bf6e18dd89e8b22dcb28d8c686ac61efdff26051ce5f1500e67f8"
    );
    assert_eq!(
        hash(Algorithm::Sha256.new_hasher(), data),
        "41ee6f59ca3f414a4b9f8cb814a32e8b83772996681b2dbe22616e2dbf46f26c"
    );
    assert_eq!(
        hash(Algorithm::Sha384.new_hasher(), data),
        "fe83f1dec840ea9ed955699715f5254b671b6e6ce6aed181cef3a144456c5412847953dd06165bbad919fa63cbdc15b2"
    );
    assert_eq!(
        hash(Algorithm::Sha512.new_hasher(), data),
        "db93bfddfd0c2d2b9fa6db6965d11ec058825768ee8176a296c49a6c0c3eb781f31d1170932f37d14e0fd70b3837d95c11b7685cb1f19b5e3673a0cf23a99339"
    );
    assert_eq!(
        hash(Algorithm::Sha3_224.new_hasher(), data),
        "660d398843f53e8123d41e5db9ee7e53639d1b137daf3306d1c06f6c"
    );
    assert_eq!(
        hash(Algorithm::Sha3_256.new_hasher(), data),
        "c4046f33ca017c78fe9fc63079e80658362431d5095d033f5780db165b01c735"
    );
    assert_eq!(
        hash(Algorithm::Sha3_384.new_hasher(), data),
        "e7768c761014ff8813e695c28a2d83b3c29994d0b7eb72d764f3adf0cc1e696d5ea5f0e71e0b9e455b61583b7afd2e5f"
    );
    assert_eq!(
        hash(Algorithm::Sha3_512.new_hasher(), data),
        "ba71be78abe42ce394b2960aad30acaba7eb5809cde04394f81c672dcc90eebda19c3c663d90f0d9738e6cc15d10236c17ae1fd1a42e07cd6bda266e74d2fe53"
    );
}

struct Decorator {
    safe: &'static str,
    moderate: &'static str,
    compromised: &'static str,
}

impl AlgorithmTextDecorator for Decorator {
    fn decorate_safe_algorithm(&self, algorithm: &str) -> String {
        format!("{} {}", self.safe, algorithm)
    }

    fn decorate_moderate_algorithm(&self, algorithm: &str) -> String {
        format!("{} {}", self.moderate, algorithm)
    }

    fn decorate_compromised_algorithm(&self, algorithm: &str) -> String {
        format!("{} {}", self.compromised, algorithm)
    }
}

fn hash(mut hasher: Hasher, data: &[u8]) -> String {
    hasher.update(data);
    hex::encode(hasher.finalize())
}

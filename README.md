# Suma - Simple checksum validator

[What is Suma](#what-is-suma) | [Features](#features) | [Screenshots](#screenshots) |
[Install](#install) | [Run](#run) | [License](LICENSE.txt) | [Changelog](doc/changelog.md)

## What is Suma?

Suma is a simple graphical application used to validate file checksums.<br>
Just run Suma, choose file to validate, select hash algorithm, paste
checksum provided by the file creator and click on "Validate".<br>
Once the validation is finished, you will know whether file is ok or damaged.

## Features

 * Supports MD5, SHA-1, SHA-2 (224, 256, 384, 512) and SHA-3 (224, 256, 384, 512) algorithms.
 * Multilingual (currently Czech, English and Polish languages are available).
 * Easy to use - has one window and one dialog. And "about" dialog, of course!
 * Open source - you can adjust this engineering marvel 😎 to your needs!

## Screenshots

![new.png](doc/screenshots/new.png)

_Suma is started or "Reset" button has been clicked_

![running.png](doc/screenshots/running.png)

_Suma is calculating checksum_

![success.png](doc/screenshots/success.png)

_Calculated checksum matched the pasted one_

![fail.png](doc/screenshots/fail.png)

_Calculated checksum did not match the pasted one_

## Install

#### Requirements

1. Build
 * Rust Edition 2021
 * Meson >= 0.59.4
 * gettext
 * GTK >= 4.8
 * Optionally, you can use provided `Makefile` for convenience.

2. Runtime
 * GTK >= 4.8
 * gettext

#### Steps

 * `mkdir build`
 * `meson setup build --buildtype release`
 * `meson compile -C build`
 * `meson install -C build`

## Run

You can launch Suma the following ways:
 * From your desktop environment's application menu
 * Using `open with`-like function on selected file in your file manager
 * From command line with `suma [file_path]`
